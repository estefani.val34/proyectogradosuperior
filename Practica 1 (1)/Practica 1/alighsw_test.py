import alighsw_lib

def test_get_similarity_1():
    assert alighsw_lib.get_similarity("A", "A", 3) == 3

def test_get_similarity_2():
    assert alighsw_lib.get_similarity("A", "T", 3) == -3

def test_get_score_1():
    adn1 = "gata"
    adn2 = "gata"
    mat = [[0, 0, 0, 0, 0], [0, 3, 1, 0, 0], [0, 1, 6, 4, 3],
           [0, 0, 4, 9, 7], [0, 0, 3, 7, 12]]
    assert alighsw_lib.get_score(adn1, adn2, mat) == ("4", "4")

def test_get_score_2():
    adn1 = "GGGAAT"
    adn2 = "TTGTAG"
    mat = [[0, 0, 0, 0, 0, 0, 0], [0, 0, 0, 3, 1, 0, 3], [0, 0, 0, 3, 1, 0, 3],
           [0, 0, 0, 3, 1, 0, 3], [0, 0, 0, 1, 0, 4, 2], [0, 0, 0, 0, 0, 3, 1],
           [0, 3, 3, 1, 3, 1, 0]]
    assert alighsw_lib.get_score(adn1, adn2, mat) == ("4", "5")
