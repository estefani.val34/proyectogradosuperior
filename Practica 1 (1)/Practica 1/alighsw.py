import argparse
import alighsw_lib

# If --help mostrar ayuda
PARSER = argparse.ArgumentParser()
# Definir la estructura de mi cmdline
PARSER.add_argument("secuencia_1", type=str)
PARSER.add_argument("secuencia_2", type=str)
PARSER.add_argument("match", type=int)
PARSER.add_argument("gap", type=int)
ARGS = PARSER.parse_args()  # analizar sys.argv

alighsw_lib.smith_waterman_algorithm(ARGS.secuencia_1, ARGS.secuencia_2, ARGS.match, ARGS.gap)
