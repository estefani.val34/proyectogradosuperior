"Este módulo es el de alineamiento global"

import argparse
import alighnw_lib
# If --help mostrar ayuda
PARSER = argparse.ArgumentParser()
# Definir la estructura de mi cmdline
PARSER.add_argument("secuencia_1", type=str)
PARSER.add_argument("secuencia_2", type=str)
PARSER.add_argument("match", type=int)
PARSER.add_argument("gap", type=int)
ARGS = PARSER.parse_args()  # analizar sys.argv

alighnw_lib.needleman_wunsch_algorithm(ARGS.secuencia_1, ARGS.secuencia_2, ARGS.match, ARGS.gap)
