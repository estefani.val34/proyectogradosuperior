import sys
import argparse
import re
import multi_fasta_lib


def main():
    PARSER = argparse.ArgumentParser()
    PARSER.add_argument("sequence1")
    PARSER.add_argument('sfile', nargs='?', type=argparse.FileType('r'), default=sys.stdin)
    PARSER.add_argument("match")
    PARSER.add_argument("gap")
    PARSER.add_argument("option")
    ARGS = PARSER.parse_args()

    sequence1 = ARGS.sequence1.upper()
    match=int(ARGS.match)
    gap=int(ARGS.gap)
    option = ARGS.option.upper()
    regex = r'^>.*\n[^>]'
    sfile = (ARGS.sfile)

    multi_fasta_lib.imprimir_alignments(sfile, regex, sequence1, match, gap, option)

main()
