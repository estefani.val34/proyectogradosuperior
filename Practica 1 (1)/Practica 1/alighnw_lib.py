"Este módulo es el de alineamiento global"

import numpy as np


def create_matrix(adn1: str, adn2: str, match: int, gap: int):
    "Crea la matriz"
    len_adn1 = len(adn1)
    len_adn2 = len(adn2)
    mat = np.zeros((len_adn1+1, len_adn2+1), int)
    mat = initialize_matriz(len_adn1, len_adn2, mat, gap)
    for i in range(len_adn1):
        for j in range(len_adn2):
            diagonal = mat[i][j]
            abajo = mat[i+1][j]
            derecha = mat[i][j+1]
            if adn1[i] == adn2[j]:
                mat[i+1][j+1] = max(diagonal+match, abajo-gap, derecha-gap)
            else:
                mat[i+1][j+1] = max(diagonal-match, abajo-gap, derecha-gap)
    return mat

def initialize_matriz(len_adn1: int, len_adn2: int, mat: np.ndarray, gap: int):
    "Inicializa la matriz"
    resultado = 0
    for i in range(len_adn1+1):
        mat[i][0] = resultado
        resultado -= gap
    resultado = 0
    for j in range(len_adn2+1):
        mat[0][j] = resultado
        resultado -= gap
    return mat

def get_similarity(a_letter: str, b_letter: str, match: int):
    "Devuelve si las letras coinciden"
    if a_letter == b_letter:
        return match
    return -match
def get_score(adn1: str, adn2: str, matriz: np.ndarray):
    "Busca la puntuación máxima"
    seq1 = len(adn1) + 1
    seq2 = len(adn2) + 1
    max_score = 0
    position = (0,0)
    for i in range(seq1):
        for j in range(seq2):
            if max_score < matriz[i][j]:
                max_score = matriz[i][j]
                position = (i, j)
    return position
def traceback(adn1: str, adn2: str, matriz: np.ndarray, match: int, gap: int):
    "Genera el alineamiento"
    aligment_a = ""
    aligment_b = ""
    aligment_c = ""
    i = len(adn1)
    j = len(adn2)
    while(i > 0 or j > 0):
        similarity = get_similarity(adn1[i-1], adn2[j-1], match)
        if i > 0 and j > 0 and matriz[i][j] == matriz[i-1][j-1] + similarity:
            aligment_a += adn1[i-1]
            aligment_b += adn2[j-1]
            if get_similarity(adn1[i-1], adn2[j-1], match) == match:
                aligment_c += "|"
            else:
                aligment_c += "*"
            i = i-1
            j = j-1
        elif(i > 0 and matriz[i][j] == matriz[i-1][j]+(-gap)):
            aligment_a += adn1[i-1]
            aligment_b += "-"
            aligment_c += " "
            i = i-1
        else:
            aligment_a += "-"
            aligment_b += adn2[j-1]
            aligment_c += " "
            j = j-1
    print("".join(reversed(aligment_a)))
    print("".join(reversed(aligment_c)))
    print("".join(reversed(aligment_b)))

def needleman_wunsch_algorithm(adn1: str, adn2: str, match: int, gap: int):
    "Algoritmo de alineamiento global"
    adn1 = adn1.upper()
    adn2 = adn2.upper()
    matriz = create_matrix(adn1, adn2, match, gap)
    postion_max = get_score(adn1, adn2, matriz)
    score = matriz[postion_max]
    print("────────────────────────────────────────────")
    print("Secuencia 1: "+adn1)
    print("Secuencia 2: "+adn2)
    print("────────────────────────────────────────────")
    traceback(adn1, adn2, matriz, match, gap)
    print("────────────────────────────────────────────")
    print("Score: "+str(score))
    print("────────────────────────────────────────────")
    print(matriz)
