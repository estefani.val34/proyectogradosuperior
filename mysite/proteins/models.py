from django.db import models

class Product(models.Model):
    name = models.CharField(max_length=255)
    price = models.DecimalField( max_digits=19, decimal_places=10)
    description = models.TextField()
    release_date = models.DateField()
   # manufacturer = models.ForeignKey(Manufacturer)