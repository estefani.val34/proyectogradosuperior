from django.shortcuts import render

from .models import Product

from .filters import ProductFilter

def product_list(request):
    f = ProductFilter(request.GET, queryset=Product.objects.all())
    return render(request, 'proteins/template.html', {'filter': f})