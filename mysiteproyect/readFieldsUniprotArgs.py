"""
https://www.uniprot.org/uniprot/P14060.txt   : uno en concreto 
https://www.uniprot.org/uniprot/P31946.txt   : solo cambiar el entry de la proteina 
https://www.uniprot.org/uniprot/Q07912.txt  : funcion catalytica , activity regulation y nucleotide binding 

para que me daescarge todos y me haga el analisis , hago un wget de cada url corresponiente a cada proteina . QUe lo haga automaticamente de una lista de descargas de proteinas 

De un fichero que ya lo tienes bajado lo añade a la base de datos del programa, por cada campo

"""


"""
conseguir information of one txt
"""
import django
import os
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "mysiteproyect.settings")
django.setup()

from proteins.models import Protein , Funtion , PBD
import re
import argparse
import sys


"""
read file txt
f = open("P29372.txt","r") 
f1=f.readlines() 
"""
PARSER = argparse.ArgumentParser()
PARSER.add_argument("infile", nargs="?", type=argparse.FileType("r"), default=sys.stdin)
ARGS = PARSER.parse_args()

with open(ARGS.infile.name, "r") as txt_file:
    f1 = txt_file.readlines()


p=Protein() 
"""
entry
"""
entry=""                            
pattern_entry=re.compile(r'AC\s+(\w+)')                      
prog_entry=re.compile(pattern_entry)                         
result_entry=prog_entry.match(f1[1])  
entry=result_entry.group(1)   

p.entry=entry 
"""
entry_name
"""
entry_name=""
pattern=re.compile(r'ID (.+) Reviewed.+')  
prog=re.compile(pattern)
result=prog.match(f1[0])  

entry_name=result.group(1).strip()

p.entry_name = entry_name 

"""
protein_names
"""
protein_names_list=[]  
protein_names=""  

pattern_protein_names=re.compile(r'DE\s+.+Full=(.+);') 

for i in range(0,len(f1)):  
      for m in re.finditer(pattern_protein_names, f1[i]):  
          if m.group(1) not in protein_names_list:  
              protein_names_list.append( m.group(1))  

protein_names=". ".join(protein_names_list)   
p.protein_names=protein_names

"""
 cross_reference_GeneID
"""
cross_reference_geneID_list=[]   
cross_reference_geneID=""   

pattern_geneid=re.compile('DR\s+GeneID;\s(\w+);')

for i in range(0,len(f1)):   
       for m in re.finditer(pattern_geneid, f1[i]):   
           if m.group(1) not in cross_reference_geneID_list:   
               cross_reference_geneID_list.append( m.group(1))   

cross_reference_geneID=" ".join(cross_reference_geneID_list) 
p.cross_reference_GeneID=cross_reference_geneID

"""
gene_names
"""
gen_name_list=[]   
gen_name=""

pattern_gen_name=re.compile('GN\s+Name=(.+);')


for i in range(0,len(f1)):   
       for m in re.finditer(pattern_gen_name, f1[i]):   
           if m.group(1) not in gen_name_list:   
               gen_name_list.append( m.group(1)) 

gen_name="".join(gen_name_list)

p.gene_names=gen_name   
  
"""
organism
"""

organism_list=[]   
organism=""

pattern_organism=re.compile('OS\s+(.+).')

for i in range(0,len(f1)):   
       for m in re.finditer(pattern_organism, f1[i]):   
           if m.group(1) not in organism_list:   
               organism_list.append( m.group(1)) 

organism="".join(organism_list)
p.organism=organism 

"""
taxonomy_lineage
"""

taxo_list=[]   

pattern_taxo=re.compile('OC\s+(.+)')

for i in range(0,len(f1)):   
       for m in re.finditer(pattern_taxo, f1[i]):   
           if m.group(1) not in taxo_list:   
               taxo_list.append( m.group(1)) 


taxo1="".join(taxo_list) 
taxo2=taxo1.split(";")  
taxo3=" ->".join(taxo2)

p.taxonomy_lineage=taxo3 

"""
 sequence
"""
seq_list=[]
pattern_seq=re.compile('SQ\s+SEQUENCE.+;')
index_inicio=0                                              

for i in range(0,len(f1)):    
        for m in re.finditer(pattern_seq, f1[i]):    
               index_inicio=i 

seq_list=f1[index_inicio+1:len(f1)-1]

seq_list_split_join=[]  
for l in seq_list: 
     seq_list_split_join.append("".join(l.split())) 

seq=""
seq= "".join(seq_list_split_join) 

p.sequence=seq 
 
"""
length
"""
length=""                            
pattern_length=re.compile(r'ID\s.+ (\d+)\s+AA')                      
prog_length=re.compile(pattern_length)                         
result_length=prog_length.match(f1[0])  
length=result_length.group(1)   

p.length=length 


"""
function
"""
#todas las proteinas tiene que tener funcion----común----
pattern_function_inicio=re.compile(r'CC\s+-!-\s(FUNCTION).+')
index_inicio_function=0                                              

for i in range(0,len(f1)):    
        for m in re.finditer(pattern_function_inicio, f1[i]):    
               index_inicio_function=i 


function_list = f1[index_inicio_function+1:] 
f_idx = [i for i, item in enumerate(function_list) if re.search('CC\s+-!-\s+.+:', item)] 
#--------------------------------------------común------

f_list=f1[index_inicio_function:index_inicio_function+1+f_idx[0]] 

function_str_list=[] 
for i in range(0,len(f_list)): 
    replaced_str=re.sub('(\\n)?CC','',f_list[i]) 
    replaced_str_2=re.sub('-!-','',replaced_str)   
    replaced_str_3=re.sub('FUNCTION:','',replaced_str_2) 
    function=replaced_str_3.strip()    
    function_str_list.append(function) 

function=""
function=" ".join(function_str_list) 


f1 = Funtion(function=function)



"""
catalytic_activity

posibilidades:
-function + catalytic activity (1 o muchas)
-function + SUBUNIT (1 o muchos)
-function + otros

"""
#-function + catalytic activity (1 o muchas)--f_idx_cata != []
f_idx_cata = [i for i, item in enumerate(function_list) if re.search(r'CC\s+-!-\s+CATALYTIC ACTIVITY:', item)] 
catalytic_activity="";

if len(f_idx_cata)!=0:
    index_fin_cata=f_idx[f_idx.index(f_idx_cata[-1])+1 ]
    cata_list_join=" ".join(function_list[f_idx_cata[0]:index_fin_cata])

    replaced_1=re.sub('(\\n)?CC','',cata_list_join)   
    replaced_2=re.sub('\\n','',replaced_1)           
    replaced_3=re.sub('\s',' ',replaced_2)   

    split_replaced_list=replaced_3.split("-!- CATALYTIC ACTIVITY:")

    cata_str_list=[] 
    for i in range(0,len(split_replaced_list)): 
        replaced_regex1=re.sub('Xref=.+ChEBI:CHEBI:\d+','',split_replaced_list[i]) 
        replaced_regex2=re.sub('Evidence={.+}','',replaced_regex1)   
        replaced_regex3=re.sub('EC=\d+.\d+.\d+.\d+;','',replaced_regex2)
        replaced_espai=re.sub(' ','',replaced_regex3)   
        cata_str_list.append(replaced_espai) 


    catalytic_activity="".join(cata_str_list) 

    f1.catalytic_activity=catalytic_activity   

else:

    f1.catalytic_activity=catalytic_activity  


"""
activity_regulation

posibilidades:
-function + activity regulation (1 o muchas)
-function + SUBUNIT (1 o muchos)
-function + otros
"""

f_idx_act = [i for i, item in enumerate(function_list) if re.search(r'CC\s+-!-\s+ACTIVITY REGULATION:', item)] 
activity_regulation="";

if len(f_idx_act)!=0:
    index_fin_act=f_idx[f_idx.index(f_idx_act[-1])+1 ]
    act_list_join=" ".join(function_list[f_idx_act[0]:index_fin_act])

    replaced_1=re.sub('(\\n)?CC','',act_list_join)   
    replaced_2=re.sub('\\n','',replaced_1)           
    replaced_3=re.sub('\s',' ',replaced_2)   

    split_replaced_list=replaced_3.split("-!- ACTIVITY REGULATION:")

    act_str_list=[] 
    for i in range(0,len(split_replaced_list)): 
        replaced_regex1=re.sub('Xref=.+ChEBI:CHEBI:\d+','',split_replaced_list[i]) 
        replaced_regex2=re.sub('Evidence={.+}','',replaced_regex1)   
        replaced_regex3=re.sub('EC=\d+.\d+.\d+.\d+;','',replaced_regex2)
        replaced_espai=re.sub(' ','',replaced_regex3)   
        act_str_list.append(replaced_espai) 


    activity_regulation="".join(act_str_list) 

    f1.activity_regulation =activity_regulation   

else:

    f1.activity_regulation =activity_regulation  


"""
subcellular_location
"""
f_idx_loc = [i for i, item in enumerate(function_list) if re.search(r'CC\s+-!-\s+SUBCELLULAR LOCATION:', item)] 
subcellular_location="";

if len(f_idx_loc)!=0:
    index_fin_loc=f_idx[f_idx.index(f_idx_loc[-1])+1 ]
    act_list_join=" ".join(function_list[f_idx_loc[0]:index_fin_loc])

    replaced_1=re.sub('(\\n)?CC','',act_list_join)   
    replaced_2=re.sub('\\n','',replaced_1)           
    replaced_3=re.sub('\s',' ',replaced_2)   

    split_replaced_list=replaced_3.split("-!- SUBCELLULAR LOCATION:")

    act_str_list=[] 
    for i in range(0,len(split_replaced_list)): 
        replaced_regex1=re.sub('Xref=.+ChEBI:CHEBI:\d+','',split_replaced_list[i]) 
        replaced_regex2=re.sub('Evidence={.+}','',replaced_regex1)   
        replaced_regex3=re.sub('EC=\d+.\d+.\d+.\d+;','',replaced_regex2)
        replaced_espai=re.sub(' ','',replaced_regex3)   
        act_str_list.append(replaced_espai) 


    subcellular_location="".join(act_str_list) 

    f1.subcellular_location =subcellular_location   

else:

    f1.subcellular_location =subcellular_location  

f1.save()

"""
seq_similarities
"""
f_idx_simi = [i for i, item in enumerate(function_list) if re.search(r'CC\s+-!-\s+SIMILARITY:', item)] 
seq_similarities="";

if len(f_idx_simi)!=0:
    #que se más grande de una fila
    if f_idx.index(f_idx_simi[-1])<len(f_idx)-1:
        index_fin_simi=f_idx[f_idx.index(f_idx_simi[-1])+1 ]
        act_list_join=" ".join(function_list[f_idx_simi[0]:index_fin_simi])
    else:
    #que este en la misma fila
        act_list_join=" ".join(function_list[f_idx_simi[0]])

    replaced_1=re.sub('(\\n)?CC','',act_list_join)   
    replaced_2=re.sub('\\n','',replaced_1)           
    replaced_3=re.sub('\s',' ',replaced_2)   
    replaced_4=re.sub(r'C C       - ! -   S I M I L A R I T Y :', '', act_list_join).strip()   
    replaced_5=re.sub('{ECO:\d+}.','',replaced_4)
    seq_similarities=replaced_5.strip()

    p.seq_similarities =seq_similarities   

else:

    p.seq_similarities =seq_similarities  

"""
Cross_reference_PDB
"""
f_idx_DR = [i for i, item in enumerate(function_list) if re.search(r'DR\s+PDB\s*', item)] 
f_DR = [i for i, item in enumerate(function_list) if re.search(r'DR\s+PDB\s*;\s*[0-9A-Z]+\s*;\s*.*', item)]  
cross_reference_PDB = ""

if len(f_DR)!=0:
    #que se más grande de una fila
    if f_idx_DR.index(f_DR[-1])<len(f_idx_DR)-1:
        index_fin_pdb=f_idx_DR[f_idx_DR.index(f_DR[-1])+1 ]
        act_list_join=" ".join(function_list[f_DR[0]:index_fin_pdb])
    else:
    #que este en la misma fila
        act_list_join=" ".join(function_list[f_DR[0]])

    rep1=re.sub('DR\s+',' ',act_list_join) 
    rep2=re.sub('\n',' ',rep1)
    rep3=rep2.strip()
    cross_reference_PDB = rep3
    p.cross_reference_PDB =cross_reference_PDB   

else:

    p.cross_reference_PDB =cross_reference_PDB  

"""
save changes in the protein
""" 
p.save() 
p.functions.add(f1) 

"""
fill model PBD
"""

lcross = cross_reference_PDB.split('PDB;')      
en = entry

for pbd in lcross: 
    if (pbd !=""): 
        id_pbd = pbd.split(";")[0].strip()
        desc = ";".join(pbd.split(";")[1:]).strip()
        PBD.objects.create(id_pbd = id_pbd, entry = en, protein = p, description = desc)  
        





