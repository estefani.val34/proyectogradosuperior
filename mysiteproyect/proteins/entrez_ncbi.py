import django
from django.core.files.storage import FileSystemStorage
import os
import re
import pathlib 
from os import listdir
from os.path import isfile, isdir
import os.path
import sys
from Bio import Entrez
from .models import Protein, PBD, Url

def search_something(TERM_SEARCH, MAX_SEARCH, USER, USER_USERNAME, DATABASE):
    os.makedirs('media/'+USER, exist_ok=True)
    urls = []
    
    Entrez.email = "estefani.val34@gmail.com"

    with Entrez.esearch(db=DATABASE,
                    term=TERM_SEARCH,
                    idtype="acc",
                    retmax=MAX_SEARCH) as response:
        RES = Entrez.read(response)

    if  RES['Count']!='0':

        ID = ",".join(RES["IdList"])
        with Entrez.epost(DATABASE, id=ID) as response:
            EPOST = Entrez.read(response)

        QUERY_KEY = EPOST['QueryKey']

        WEBENV = EPOST['WebEnv']

        ACC_ARR = RES['IdList']
        ACC_ARR2 = ACC_ARR.copy()

        with Entrez.efetch(db=DATABASE,
                        retmode="text",
                        rettype="gb",
                        retstart=0,
                        retmax=1,
                        webenv=WEBENV,
                        query_key=QUERY_KEY,
                        idtype="acc") as response:
            ACC_ARR[0] = response.read()

        LENGHT = len(ACC_ARR)
        for i in range(1, LENGHT, 1):
            with Entrez.efetch(db=DATABASE,
                            retmode="text",
                            rettype="gb",
                            retstart=i,
                            retmax=i,
                            webenv=WEBENV,
                            query_key=QUERY_KEY,
                            idtype="acc") as response:
                ACC_ARR[i] = response.read()

        LENGHT2 = len(ACC_ARR2)
        fs = FileSystemStorage()

        for i in range(LENGHT2):
            with open(os.path.join('media/'+USER,ACC_ARR2[i]+'.gb'), 'w') as gb_file:
                path = os.path.join('media/'+USER,ACC_ARR2[i]+'.gb')
                gb_file.write(ACC_ARR[i])
                #name = fs.save(gb_file.name, gb_file)
                url = fs.url(gb_file.name)
                Url.objects.create(title=ACC_ARR2[i],user=USER_USERNAME,gb=path)
                urls.append(url)
    else:
        urls.append("Not found results.")
    return urls


def search_something_structure(TERM_SEARCH, MAX_SEARCH, USER, DATABASE):
    os.makedirs('media/'+USER, exist_ok=True)
    urls = []
    
    Entrez.email = "estefani.val34@gmail.com"

    with Entrez.esearch(db=DATABASE,
                    term=TERM_SEARCH,
                    idtype="acc",
                    retmax=MAX_SEARCH) as response:
        RES = Entrez.read(response)

    if  RES['Count']!='0':
        urls = RES["IdList"]
    else:
        urls.append("Not found results.")
    return urls



if __name__ == '__main__':
    try:
        arg = sys.argv[1]
        arg2 = sys.argv[2]
        arg3 = sys.argv[3]
        arg4 = sys.argv[4]
        arg5 = sys.argv[5]
    except IndexError:
        arg = None
        arg2 = None
        arg3 = None
        arg4 = None
        arg5 = None

    return_val = search_something(arg,arg2,arg3,arg4,arg5)