"""
Test

References:

Django: https://docs.djangoproject.com/en/3.0/topics/testing/tools/

Automatic Test : https://www.selenium.dev/

Command Run Tests : ./manage.py test proteins.tests

"""
import pytest
from django.core.management import call_command
from django.test import TestCase
from django.contrib.staticfiles.testing import StaticLiveServerTestCase
from selenium.webdriver.firefox.webdriver import WebDriver
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from proteins.models import Url
# Create your tests here.

# vamos a hacer los tests del client

# test de la base de datos
# comprovar que si creo un objeto en el modelo Url se lista en la lista en el historial

# Selinum
# asumiremos que está utilizando la aplicación staticfiles y desea que se sirvan
# los archivos estáticos durante la ejecución de sus pruebas de forma similar a lo
# que obtenemos en el momento del
# desarrollo con DEBUG = True, es decir, sin tener que recopilarlos mediante collectstatic.



class MySeleniumTests(StaticLiveServerTestCase):
    """
    Automatically open Firefox then go to the login page, enter the
    credentials and press the “Log in” button.
    """
  
    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        cls.selenium = WebDriver()
        cls.selenium.implicitly_wait(10)

    @classmethod
    def tearDownClass(cls):
        cls.selenium.quit()
        super().tearDownClass()

    def test_login(self):
        self.selenium.get('%s%s' % (self.live_server_url, '/login'))
        username_input = self.selenium.find_element_by_name("username")
        username_input.send_keys('myuser')
        password_input = self.selenium.find_element_by_name("password")
        password_input.send_keys('secret')
        self.selenium.find_element_by_xpath('//input[@value="Log in"]').click()


class SimpleTest(TestCase):
    """
    Client Test of urls : list and urls. 
    """
    def test_list(self):
        response = self.client.get('/list')
        self.assertEqual(response.status_code, 200)

    def test_urls(self):
        response = self.client.get('/urls')
        self.assertEqual(response.status_code, 200)

    def test_multiple(self):
        with open('opuntia.fasta') as fp:   
            c.post('/align/multiple', {'gb': fp})  


# Testear que la lista de Urls es la misma que la vista /Urls
class UrlTestCase(TestCase):
    """
    Test if all are deleted Urls. 
    """
    def setUp(self):
        self.test_loading_and_dumping()

    def test_loading_and_dumping(self):
        Url.objects.all().delete()
        self.assertQuerysetEqual(Url.objects.all(),[])
