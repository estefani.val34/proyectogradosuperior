import os
from django.shortcuts import render
from django.shortcuts import redirect
from django.contrib.auth import logout as do_logout
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.contrib.auth import authenticate
from django.contrib.auth.forms import AuthenticationForm
from django.contrib.auth import login as do_login
from django.contrib.auth.forms import UserCreationForm
from django.views.generic import ListView, DetailView
from django.core.files.storage import FileSystemStorage
from .forms import ProteinForm, NCBIForm, UrlForm, AlighnmentForm, AlighnmentMultipleForm
from .models import Protein, PBD, Url
# from django.contrib.auth.models import User
from .filters import PbdFilter
from .entrez_ncbi import search_something, search_something_structure
from align.alighnw import align_NW
from align.alighsw import align_SW
from align.multialignfasta import generate_aln_dnd, write_file_sequences, delete_dir, validate_file, is_fasta_extension, len_seq_ids, empty_sequences
#from align.alighnw_lib import needleman_wunsch_algorithm

from django.http import HttpResponse


def read_file(request, pk):
    """
    Open window 
    """
    obj = Url.objects.get(pk=pk)
    path = str(obj.gb)
    f = open(path, 'r')
    file_content = f.read()
    f.close()
    return HttpResponse(file_content, content_type="text/plain")


def search_term_ncbi(request):
    form = NCBIForm()
    uploaded_file = []
    uploaded_file_structure = []
    list_media_user = []
    if request.method == "POST":
        form = NCBIForm(data=request.POST)
        if form.is_valid():
            term = form.cleaned_data['term']
            num = form.cleaned_data['num_downloads']
            database = form.cleaned_data['database']
            user = "user_"+str(request.user.id)
            user_username = str(request.user)
            if database == 'structure':
                uploaded_file_structure = search_something_structure(
                    term, num, user, database)
            else:
                uploaded_file = search_something(
                    term, num, user, user_username, database)

    return render(request, 'proteins/term_ncbi_search.html',
                  {'form': form,  'urls': uploaded_file, 'uploaded_file_structure': uploaded_file_structure})


def urls_list(request):
    user = str(request.user)
    urls = Url.objects.all().filter(user=user)
    return render(request, 'proteins/urls_list.html', {
        'urls': urls
    })


def urls_upload(request):
    if request.method == 'POST':
        form = UrlForm(request.POST, request.FILES)

        if form.is_valid():
            post = form.save(commit=False)
            post.user = request.user
            post.sequences = None
            post.save()
            return redirect('urls_list')
    else:
        form = UrlForm()
    return render(request, 'proteins/urls_upload.html', {
        'form': form
    })


def pbd_list(request):
    protein_list = PBD.objects.all()
    f = PbdFilter(request.GET, queryset=protein_list)

    paginator = Paginator(f.qs, 7)
    page = request.GET.get('page')

    try:
        proteins = paginator.page(page)
    except PageNotAnInteger:
        proteins = paginator.page(1)
    except EmptyPage:
        proteins = paginator.page(paginator.num_pages)

    return render(request, 'proteins/pbd_list.html', {
        'title': 'Home',
        'proteins': proteins,
        'page': page,
        'filter': f,
    })


def delete_url(request, pk):
    if request.method == 'POST':
        url = Url.objects.get(pk = pk, user = request.user)
        url.delete()
    path_refered = str(request.META.get('HTTP_REFERER'))
    return redirect(path_refered)


def delete_all_urls(request):
    if request.method == 'POST':
        url = Url.objects.all().filter(user = request.user)
        url.delete()
        delete_dir('media/user_'+str(request.user.id))
    path_refered = str(request.META.get('HTTP_REFERER'))
    return redirect(path_refered)


def align_multiple_list(request, gb):
    urls = Url.objects.all().filter(gb__contains = gb, user = request.user)

    return render(request, 'proteins/align_multiple_list.html', {
        'urls': urls, 'gb': gb
    })


def align_multiple_sequence(request):
    errornogb = ""
    errorseq = " "
    errorfile = ""
    errorlen = ""
    if request.method == 'POST':
        form = AlighnmentMultipleForm(request.POST, request.FILES)
        seq = request.POST.get('sequences', '')
        print(seq)
        gb = request.FILES.get('gb', '')

        if seq == "" or seq == None:
            if gb == "" or gb == None:
                errornogb = "Please enter a File or Sequences."
            else:
                if is_fasta_extension(str(gb)) == True:
                    if form.is_valid():
                        fil = request.FILES['gb']
                        sequences = form.cleaned_data['sequences']
                        post = form.save(commit=False)
                        post.title = str(fil)
                        post.types = 'MultiFasta'
                        post.user = request.user
                        post.save()
                       # validate que el contenido de las secuencias no este vacio
                        if empty_sequences(str(post.gb)) == True:
                            if validate_file(str(post.gb)) == True:
                                tup_files = generate_aln_dnd(
                                    str(post.gb), request.user)
                                print (tup_files)
                                return redirect('align_multiple_list', gb = tup_files[1]+'.')
                            else:
                                errorseq = "The number of sequences must be greater than or equal to two and the name of the sequences must be unique. "
                        else:
                            errorlen = "A sequence cannot be empty."
                else:
                    errorfile = "The file must be in fasta format."
        else:
            path_gb = write_file_sequences(seq, request.user)
            if empty_sequences(str(path_gb)) == True:
                if validate_file(str(path_gb)) == True:
                    tup_files = generate_aln_dnd(str(path_gb), request.user)
                    return redirect('align_multiple_list', gb=tup_files[1])
                else:
                    errorseq = "The number of sequences must be greater than or equal to two and the name of the sequences must be unique. "
            else:
                errorlen = "A sequence cannot be empty."
    else:
        form = AlighnmentMultipleForm()
    return render(request, 'proteins/align_multiple_sequence.html', {'form': form,
                                                                     'errornogb': errornogb, 'errorseq': errorseq, 'errorfile': errorfile, 'errorlen': errorlen})


def align_sequence(request):
    form = AlighnmentForm()
    alignment = []
    alignment_1_names = []
    alignment_2_traceback = []
    alignment_3_matrix = []
    match = ""
    gap = ""
    if request.method == 'POST':
        form = AlighnmentForm(request.POST)
        if form.is_valid():
            secuencia1 = form.cleaned_data['secuencia1']
            secuencia2 = form.cleaned_data['secuencia2']
            align = form.cleaned_data['align']

            if align == "1":
                match = 1
                gap = 1
                alignment = align_NW(secuencia1, secuencia2)
                alignment_1_names = alignment[0:2]
                alignment_2_traceback = alignment[2]
                alignment_3_matrix = alignment[3]

            else:  # align == "2":
                alignment = align_SW(secuencia1, secuencia2, 3, 2)
                if alignment[0] != "There is no similarity.":
                    match = 3
                    gap = 2
                    alignment_1_names = alignment[0:2]
                    alignment_2_traceback = alignment[2]
                    alignment_3_matrix = alignment[3]
                else:
                    return render(request, 'proteins/align_sequence.html', {'form': form, 'alignment': alignment,   'alignment_1': alignment_1_names,
                                                                            'alignment_2': alignment_2_traceback, 'alignment_3': alignment_3_matrix,
                                                                            'match': match, 'gap': gap})

    return render(request, 'proteins/align_sequence.html', {'form': form, 'alignment_1': alignment_1_names,
                                                            'alignment_2': alignment_2_traceback, 'alignment_3': alignment_3_matrix,
                                                            'match': match, 'gap': gap})


# def pbd_list(request):
#     f = PbdFilter(request.GET, queryset=PBD.objects.all())

#     return render(request, 'proteins/pbd_list.html', {'filter': f})


class ProteinListView(ListView):
    model = Protein
    template_name = 'proteins/protein_list.html'
    context_object_name = 'proteins'  # Default: object_list
    paginate_by = 6
    queryset = Protein.objects.all()


def protein_new(request):
    form = ProteinForm()
    return render(request, 'proteins/protein_edit.html', {'form': form})


def logout(request):
    do_logout(request)
    return redirect('/')


def register(request):
    form = UserCreationForm()
    if request.method == "POST":
        form = UserCreationForm(data=request.POST)
        if form.is_valid():
            user = form.save()
            if user is not None:
                do_login(request, user)
                return redirect('/')
    form.fields['username'].help_text = None
    form.fields['password1'].help_text = None
    form.fields['password2'].help_text = None

    return render(request, "proteins/register.html", {'form': form})


def login(request):
    form = AuthenticationForm()
    if request.method == "POST":
        form = AuthenticationForm(data=request.POST)
        if form.is_valid():
            username = form.cleaned_data['username']
            password = form.cleaned_data['password']
            user = authenticate(username=username, password=password)
            if user is not None:
                do_login(request, user)
                return redirect('/')
    return render(request, "proteins/login.html", {'form': form})
