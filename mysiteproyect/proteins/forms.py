from django import forms
from .models import Protein, Funtion, Url
from django.forms import ModelForm
from django.utils.translation import gettext_lazy as _

CHOICES = [('1', 'global_Needleman_Wunsch'), ('2', 'local_Smith_Waterman')]

class ProteinForm(forms.ModelForm):
    functions = forms.ModelMultipleChoiceField(queryset=Funtion.objects.all(), 
    widget=forms.Select(), required=False)

    class Meta:
        model = Protein
        fields = ('__all__')
        exclude=['created_date']


class NCBIForm(forms.Form):
    term = forms.CharField(label='Term', max_length=100, required=True)
    num_downloads = forms.IntegerField(label='Number of downloads', required=True, min_value=1 )
    database = forms.CharField(max_length=100, required=True )

class UrlForm(forms.ModelForm):
    class Meta:
        model = Url
        fields = ('title','gb')

        labels = {
            'gb': _('File'),
        }
        
class AlighnmentForm(forms.Form):
    secuencia1 = forms.CharField(widget=forms.Textarea,required=True)
    secuencia2 = forms.CharField(widget=forms.Textarea,required=True)
    align = forms.ChoiceField(widget=forms.RadioSelect, choices=CHOICES)

class AlighnmentMultipleForm(forms.ModelForm):
    gb = forms.FileField(required=False, label='File')
    
    class Meta:
        model = Url
        fields = ('sequences','gb',)

        labels = {
            'gb': _('File'),
        }
        