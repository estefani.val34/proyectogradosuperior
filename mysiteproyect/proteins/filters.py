import django_filters
from .models import PBD


class PbdFilter(django_filters.FilterSet):
    class Meta:
        model = PBD
        fields = {
            'protein': ['exact'],
            'entry':['icontains'],
        }