from django.db import models
from django.utils import timezone

# vamoa a rellenar los datos del script de python 

# una función puede estar en varios objetos de proteina y una
# proteina tiene varios objetos de funcion:

class Funtion(models.Model):
    function = models.TextField()
    catalytic_activity= models.TextField()
    activity_regulation	=models.TextField()
    subcellular_location=models.TextField()

    def __str__(self):
        return '%s ' % (self.function)

class Protein(models.Model):
    entry = models.CharField(max_length=50,primary_key=True,unique=True)
    entry_name = models.CharField(max_length=50, unique=True)
    protein_names = models.CharField(max_length=800)
    cross_reference_GeneID=models.CharField(max_length=20)
    gene_names = models.CharField(max_length=200)
    organism = models.CharField(max_length=80)
    taxonomy_lineage = models.CharField(max_length=300)
    sequence = models.TextField()
    length = models.PositiveIntegerField()
    seq_similarities = models.CharField(max_length = 300)
    functions = models.ManyToManyField(Funtion)
    created_date = models.DateTimeField(default=timezone.now)
    cross_reference_PDB = models.TextField()

    class Meta:
        ordering = ['entry']

    def __str__(self):
        return '%s %s' % (self.entry, self.entry_name)


#justificar porque tiene que crearse una cuenta, tiene que haber un feeback (comentarios) y consultas que ha hecho el usuario
# crear una tabla de feedback

# tabla de pbd

class PBD(models.Model):
    id_pbd = models.CharField(max_length=100, primary_key=True, unique=True)
    entry = models.CharField(max_length=200)
    protein = models.ForeignKey(Protein,on_delete=models.CASCADE)
    description =   models.CharField(max_length=300)
    created =  models.DateField(default=timezone.now)

    def __str__(self):
        return '%s' % (self.id_pbd)

    class Meta:
        ordering =('created',)
#alineamiento 
#introducir 2 entry's, hacer el alineamiento , mostrarlo, homologas ?

def user_directory_path(instance, filename):
    # file will be uploaded to MEDIA_ROOT/user_<id>/<filename>
    return 'media/'+'user_{0}/{1}'.format(instance.user.id, filename)

class Url(models.Model):
    title = models.CharField(max_length = 100)
    types = models.CharField(max_length = 100, blank = True, null = True)
    user = models.CharField(max_length = 100)
    gb = models.FileField(upload_to = user_directory_path, unique = True)
    sequences = models.TextField(blank = True, null = True)
    created_date =  models.DateField(default = timezone.now)

    def __str__(self):
        return '%s' % (self.title)
    class Meta:
        ordering =('created_date',)

    def delete(self, *args, **kwargs):
        self.gb.delete()
        super().delete(*args, **kwargs)

    def display_text_file(self):
        originalContent = None
        with open(self.gb.path, 'r+') as fp:
            originalContent = fp.read()
        
        with open(self.gb.path, 'w') as fp:
            fp.seek(0,0)
            fp.write('\n') 
            fp.write(originalContent)
            
        with open(self.gb.path, 'r+') as fp:
            return fp.read()

