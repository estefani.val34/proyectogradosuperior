#!/usr/bin/env python
"""
Program which recreates the Needleman Wunsch alignment.
    1.- Creates a Needleman Wunsch matrix from two secuences inserted by user
    2.- Using the Needleman Wunsch matrix, creates the alignment returned in 3 strings.
"""
import numpy as np


def get_matrix(secuencia1: str, secuencia2: str):
    """create the matrix of sequence 1 i of sequence 2-->matrix"""
    filas = len(secuencia2)
    columnas = len(secuencia1)
    matrix = np.zeros([filas+1, columnas+1], int)
    penalty = -1
    for i in range(filas+1):
        matrix[i, 0] = penalty*i 
    for j in range(columnas+1):
        matrix[0, j] = penalty*j 
    for j in range(columnas):
        for i in range(filas):
            letra2 = secuencia2[i]
            letra1 = secuencia1[j]
            if letra2 == letra1:
                concurrence = 1
            else:
                concurrence = -1
            fat = max(matrix[i, j]+concurrence, matrix[i+1, j]+penalty, matrix[i, j+1]+penalty)
            matrix[i+1, j+1] = fat
    return matrix

def get_max_score(matrix: np.ndarray):
    """Return max score"""
    score = matrix[matrix.shape[0]-1, matrix.shape[1]-1]
    return score


def get_alignment(matrix: np.ndarray, secuencia1: str, secuencia2: str):
    """Return alignment--> 3 strings"""
    result = []
    i = len(secuencia2)
    j = len(secuencia1)
    string1 = ""
    string1_list = []
    string2 = ""
    string2_list = []
    string3 = ""
    string3_list = []

    penalty = -1
    while(i > 0 or j > 0):
        if secuencia2[i-1] == secuencia1[j-1]:
            concurrence = 1
        else:
            concurrence = -1
        if i > 0 and j > 0 and matrix[i, j] == matrix[i-1, j-1]+concurrence:
            string1 = string1+secuencia1[j-1]
            string1_list.append((string1+secuencia1[j-1]).upper())
            string2 = string2+secuencia2[i-1]
            string2_list.append((string2+secuencia2[i-1]).upper())
            string3 = string3+"|"
            string3_list.append((string3+"|").upper())
            i = i-1
            j = j-1
        elif i > 0 and matrix[i, j] == matrix[i-1, j]+penalty:
            string1 = string1+"-"
            string1_list.append((string1+"-").upper())
            string2 = string2+secuencia2[i-1]
            string2_list.append((string2+secuencia2[i-1]).upper())
            string3 = string3+" "
            string3_list.append((string3+" ").upper())
            i = i-1
        else:
            string1 = string1+secuencia1[j-1]
            string1_list.append((string1+secuencia1[j-1]).upper())
            string2 = string2+"-"
            string2_list.append((string2+"-").upper())
            string3 = string3+" "
            string3_list.append((string3+" ").upper())
            j = j-1
    print(string1[::-1])
    print(string3[::-1])
    print(string2[::-1])
    result = [string1[::-1],string3[::-1],string2[::-1]]
    result = [x.upper() for x in result]
    return result
