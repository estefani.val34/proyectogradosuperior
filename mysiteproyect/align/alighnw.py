"Este módulo es el de alineamiento global"

from .alighnw_lib import get_matrix,get_max_score,get_alignment


def align_NW(secuencia_1, secuencia_2):
    result = []
    M = get_matrix(secuencia_1, secuencia_2)
    result = [secuencia_1.upper(), secuencia_2.upper(), get_alignment(M, secuencia_1, secuencia_2), M ]
    return result

