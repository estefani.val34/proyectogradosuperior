import alighnw_lib

def test_get_similarity_1():
    assert alighnw_lib.get_similarity("T", "T", 3) == 3

def test_get_similarity_2():
    assert alighnw_lib.get_similarity("U", "T", 3) == -3

def test_get_score_1():
    adn1 = "GGGGGAAAAA"
    adn2 = "GGGGAAGGT"
    mat = [[0, -2, -4, -6, -8, -10, -12, -14, -16, -18], [-2, 3, 1, -1, -3, -5, -7, -9, -11, -13],
           [-4, 1, 6, 4, 2, 0, -2, -4, -6, -8], [-6, -1, 4, 9, 7, 5, 3, 1, -1, -3],
           [-8, -3, 2, 7, 12, 10, 8, 6, 4, 2], [-10, -5, 0, 5, 10, 9, 7, 11, 9, 7],
           [-12, -7, -2, 3, 8, 13, 12, 10, 8, 6], [-14, -9, -4, 1, 6, 11, 16, 14, 12, 10],
           [-16, -11, -6, -1, 4, 9, 14, 13, 11, 9], [-18, -13, -8, -3, 2, 7, 12, 11, 10, 8],
           [-20, -15, -10, -5, 0, 5, 10, 9, 8, 7]]
    assert alighnw_lib.get_score(adn1, adn2, mat) == ("7", "6")
def test_get_score_2():
    adn1 = "gata"
    adn2 = "gata"
    mat = [[0, -2, -4, -6, -8], [-2, 3, 1, -1, -3],
           [-4, 1, 6, 4, 2], [-6, -1, 4, 9, 7],
           [-8, -3, 2, 7, 12]]
    assert alighnw_lib.get_score(adn1, adn2, mat) == ("4", "4")
