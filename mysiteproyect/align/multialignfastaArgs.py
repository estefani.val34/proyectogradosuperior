"""
Multiple alignment whith Clustal 

References:
Multiple sequence alignment :
https://en.wikipedia.org/wiki/Multiple_sequence_alignment

The effect of the guide tree on multiple sequence alignments and subsequent phylogenetic analyses :
https://www.ncbi.nlm.nih.gov/pubmed/18229674

Biopython :
http://biopython.org/DIST/docs/tutorial/Tutorial.html#htoc82
"""

import sys
import argparse
import os
import re
from Bio.Align.Applications import ClustalwCommandline
from Bio import AlignIO
from Bio import Phylo
from Bio.Phylo.PhyloXML import Phylogeny

PARSER = argparse.ArgumentParser()
PARSER.add_argument('infile', nargs='?', type=argparse.FileType('r'), default=sys.stdin)
ARGS = PARSER.parse_args()

"""
Generate files  .aln (multiple alignment) and  .dnd (guide tree). 
"""
clustalw_exe=r"/usr/bin/clustalw"
clustalw_cline = ClustalwCommandline(clustalw_exe, infile=ARGS.infile.name)
print(clustalw_cline.outfile)
assert os.path.isfile(clustalw_exe), "Clustal W executable missing"
stdout, stderr = clustalw_cline()
#knock name output files by create a field in the model Url of the database
print(clustalw_cline())

# print ( stderr == "")
# print("*****")
"""
Read alignment of the file .aln. Print on screen.
"""

namefasta = ARGS.infile.name

namefile = ""                            
pattern_aln=re.compile(r'(.+)\.')                      
prog_aln=re.compile(pattern_aln)                         
result_aln=prog_aln.match(namefasta)  
namefile=result_aln.group(1)  

# print('getcwd:      ', os.getcwd())
target_path_1 = os.path.join(os.path.dirname(__file__), namefile+'.aln')

# with open(target_path_1, "r") as txt_file:
#     faln = txt_file.readlines()

align = AlignIO.read(target_path_1, "clustal")
print(align.format("clustal"))

"""
Guide tree. 
"""
tree = Phylo.read(namefile+'.dnd', "newick")
Phylo.draw_ascii(tree)
#tree.rooted = True
#Phylo.draw(tree)

# tree = Phylogeny.from_tree(tree)
# tree.root.color = "blue"
# Phylo.draw(tree)





 # update the file name to make it unique
            # ob = Url.objects.get(pk = post.id)
            # namedir = str (os.path.dirname(str(post.gb)))
            # ob.gb = namedir+"/"+str(ob.id)+"_"+str(fil)
            # ob.save()
            # os.rename(r'C:\Users\Ron\Desktop\Test\Products.txt',r'C:\Users\Ron\Desktop\Test\Shipped Products.txt')