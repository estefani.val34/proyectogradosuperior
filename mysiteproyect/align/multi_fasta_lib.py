import re
import alighsw_lib
import alighnw_lib

def secuencia_en_array(archivo, regex):
    "Busca la secuencia usando una regex"
    archivo = open(archivo.name, "r")
    txt = archivo.read()
    pat = re.compile(regex, re.MULTILINE)
    #Ponemos una señal de separacion de cada secuencia
    txt_sub = pat.sub("-", txt)
    ##Hacemos una lista  donde hay un guion
    list_txt = txt_sub.split("-")
    #Hamos un array para guardar las secuencias
    list_seq = []
    for i in range(len(list_txt)):
        if list_txt[i] != '':
            list_seq.append(list_txt[i].replace('\n', ''))
    archivo.close()
    return list_seq
def imprimir_alignments(sfile, regex, sequence1, match, gap, option):
    list_seq2 = secuencia_en_array(sfile, regex)
    if option == "SW":
        for sequence in list_seq2:
            sequence2 = sequence
            print("\n********************************************")
            print("******** ALIGNMENT "+ "Smith Waterman **********")
            alighsw_lib.smith_waterman_algorithm(sequence1, sequence2, match, gap)
    elif option == "NW":
        for sequence in list_seq2:
            sequence2 = sequence
            print("\n********************************************")
            print("******** ALIGNMENT "+ "Needleman Wunsch **********")
            alighnw_lib.needleman_wunsch_algorithm(sequence1, sequence2, match, gap)
    else:
        print("The options [sw/nw]")
