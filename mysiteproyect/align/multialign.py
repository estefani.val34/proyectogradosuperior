import sys
import argparse
import re
from Bio import AlignIO
from Bio.Alphabet import IUPAC, Gapped
from Bio.Align import MultipleSeqAlignment

PARSER = argparse.ArgumentParser()
PARSER.add_argument('infile', nargs='?', type=argparse.FileType('r'), default=sys.stdin)
ARGS = PARSER.parse_args()

LIST_SEQ2 = []
LIST_IDS = []

F = open(ARGS.infile.name, "r")
TXT = F.read()

LIST_IDS = re.findall('>(.*)\n', TXT)

REGEX = r'^>(.*)[^>]'
PAT = re.compile(REGEX, re.MULTILINE)
TXT_SUB = PAT.sub("*", TXT)
LIST_TXT_SPLIT = TXT_SUB.split("*")

LENGHT = len(LIST_TXT_SPLIT)
for i in range(LENGHT):
    if LIST_TXT_SPLIT[i] != '':
        LIST_SEQ2.append(LIST_TXT_SPLIT[i].replace('\n', ''))

F.close()

LENGHTL = len(LIST_IDS)

align = AlignIO.read(ARGS.infile.name, "fasta")
print(align)
#print(LIST_SEQ2)
# align = MultipleSeqAlignment([], Gapped(IUPAC.IUPACProtein , "-"))
# for i in range(LENGHTL):
#     align.add_sequence(LIST_IDS[i], LIST_SEQ2[i])

with open("aligne.fasta", "w") as txt_file:
    txt_file.write(str(align))
