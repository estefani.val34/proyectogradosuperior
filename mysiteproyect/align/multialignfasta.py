"""
Multiple alignment whith Clustal 

References:
Multiple sequence alignment :
https://en.wikipedia.org/wiki/Multiple_sequence_alignment

The effect of the guide tree on multiple sequence alignments and subsequent phylogenetic analyses :
https://www.ncbi.nlm.nih.gov/pubmed/18229674

Biopython :
http://biopython.org/DIST/docs/tutorial/Tutorial.html#htoc82

FASTA file :
https://en.wikipedia.org/wiki/FASTA_format
"""

import sys
import argparse
import os
import re
import shutil
import random
import string
from Bio.Align.Applications import ClustalwCommandline
from Bio import AlignIO
from Bio import Phylo
from Bio.Phylo.PhyloXML import Phylogeny
from proteins.models import Url
from Bio import SeqIO

def generate_aln_dnd(file_name: str, user):
    """
    Generate files  .aln (multiple alignment) and  .dnd (guide tree). 
    """
    result = False
    clustalw_exe=r"/usr/bin/clustalw"
    clustalw_cline = ClustalwCommandline(clustalw_exe, infile=file_name)
    assert os.path.isfile(clustalw_exe), "Clustal W executable missing"
    stdout, stderr = clustalw_cline()
    thistuple = clustalw_cline()
    tuples0 = str(thistuple[0])
    tuple_names = get_names_aln_dnd(tuples0) 
    #knock name output files by create a field in the model Url of the database
    #dnd
    Url.objects.create(title = get_title_file(tuple_names[0]) , types = 'Guide Tree',  user = user , gb = tuple_names[0] , sequences = None )
    #aln
    Url.objects.create(title = get_title_file(tuple_names[1]) , types = 'Alignments', user = user , gb = tuple_names[1] , sequences = None )
  
    name_title = get_name_title_file( get_title_file(tuple_names[0]) )
    if  stderr == "":
        result =  True
    return (result, name_title )

def get_names_aln_dnd(cadena: str):
    listresult = []
    name1_search_tuple = re.search(r'file\s+created:\s+(\[.+\.dnd\])', cadena).group(1)
    listresult.append(name1_search_tuple)
    name2_search_tuple = re.search(r'CLUSTAL-Alignment\s+file\s+created\s+(\[.+\.aln\])', cadena).group(1)
    listresult.append(name2_search_tuple)

    list_names_split = [ n.split('[') for n in listresult]
    l_name1 = list_names_split [0][1].split(']')[0]
    l_name2 = list_names_split [1][1].split(']')[0]
    return (l_name1, l_name2)


def get_title_file (path: str):
    name=""                            
    pattern_name=re.compile(r'media\/.+\/(.+)')                      
    prog_name=re.compile(pattern_name)                         
    result_name=prog_name.match(path)  
    name=result_name.group(1)   
    return name

def get_name_title_file(path :str):
    name=""                            
    pattern_name=re.compile(r'(.+)\.dnd')                      
    prog_name=re.compile(pattern_name)                         
    result_name=prog_name.match(path)  
    name=result_name.group(1)   
    return name


def randomString(stringLength=8):
    """
    Generate random names unique 
    """
    letters = string.ascii_lowercase
    return ''.join(random.choice(letters) for i in range(stringLength))


def write_file_sequences(sequences : str, user):
    file_suffix = randomString(15)
    name_file = 'seq_' + str(user.id) + file_suffix + '.fasta'
    path_file = 'media/user_' + str(user.id) + '/'+ name_file

    with open(path_file, 'w') as fp:
        fp.write(sequences) 
    #create file in the model 
    Url.objects.create(title = name_file , types = 'MultiFasta',  user = user , gb = path_file , sequences = sequences )
    
    return path_file


def delete_dir (path : str):
    """
    delete all files of directory
    """
    dir_path =  path 

    try:
        shutil.rmtree(dir_path)
    except OSError as e:
        return ("Error: %s : %s" % (dir_path, e.strerror))


def validate_file(filename: str):
    """
    validate if file have 2 sequences minim (») and are unique names and is format .fasta
    """
    if is_fasta(filename) == True and num_seq(filename) == True :
        return True
    else:
        return False


def is_fasta(filename: str):
    """
    Format .fasta
    """
    with open(filename, "r") as handle:
        fasta = SeqIO.parse(handle, "fasta")
        return any(fasta)

def is_fasta_extension (name_file: str):
    """
    Check if the file extension is .fasta, .fa, .fna, .ffn, .faa, .frn
    """
    Extensions = ['fasta', 'fa', 'fna', 'ffn', 'faa', 'frn'] 
    Valid = False 
    name = ""                             
    for ext in Extensions : 
        pattern_name = re.compile(r'.+(\.'+ext+')$')         
        prog_name = re.compile(pattern_name)                 
        result_name = prog_name.match(name_file)   
         
        if result_name != None: 
            Valid = True 
            return Valid 
    return Valid 


def empty_sequences (filename : str):
    """
    Check if sequences are empty.
    """

    F = open(filename, "r")
    TXT = F.read()

    result = len_seq_ids(TXT)

    F.close()

    return result


def len_seq_ids(TXT: str):
    """
    Check if the number of times the character > is equal to the number of 
    sequences (the text between two characters >)
    """
    LIST_SEQ2 = []
    LIST_SEQ3 = []
    LIST_IDS = []
    LIST_IDS = re.findall('>(.*)\n', TXT)

    REGEX = r'^>(.*)[^>]'
    PAT = re.compile(REGEX, re.MULTILINE)
    TXT_SUB = PAT.sub("*", TXT)
    LIST_TXT_SPLIT = TXT_SUB.split("*")

    LENGHT = len(LIST_TXT_SPLIT)
    for i in range(LENGHT):
        if LIST_TXT_SPLIT[i] != '':
            LIST_SEQ2.append(LIST_TXT_SPLIT[i].replace('\n', ''))

    for i in range(len(LIST_SEQ2)):
        if LIST_SEQ2[i] != '':
            LIST_SEQ3.append( LIST_SEQ2[i] )
    
    if len(LIST_IDS) == len(LIST_SEQ3):
        return True
    else:
        return False
    


def num_seq (filename: str):
    """
    The number of sequences in the file is given by the number of times the character appears >
    """
    Valid = False
    numseq = 0 
    list_names_seq = [] 
    for line in open(filename): 
        line = line.rstrip() 
        if re.match("^>",line): 
            numseq += 1 
            list_names_seq.append (line) 

    if unique_values(list_names_seq) == True and numseq  >= 2  :
        Valid = True
    
    return Valid
         
def unique_values(g):
    """
    The content after the character  > must be unique
    """
    s = set()
    for x in g:
        if x in s: return False
        s.add(x)
    return True