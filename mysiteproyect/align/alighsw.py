"Este módulo es el de alineamiento local"

#import argparse
#import alighsw_lib
from .alighsw_lib import smith_waterman_algorithm
# If --help mostrar ayuda
# PARSER = argparse.ArgumentParser()
# # Definir la estructura de mi cmdline
# PARSER.add_argument("secuencia_1", type=str)
# PARSER.add_argument("secuencia_2", type=str)
# PARSER.add_argument("match", type=int)
# PARSER.add_argument("gap", type=int)
# ARGS = PARSER.parse_args()  # analizar sys.argv

def align_SW(secuencia_1, secuencia_2, match, gap):
    return smith_waterman_algorithm(secuencia_1, secuencia_2, match, gap)
