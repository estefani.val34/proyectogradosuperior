from django.shortcuts import render, redirect
from django.views.generic import TemplateView, ListView, CreateView
from django.core.files.storage import FileSystemStorage
from django.urls import reverse_lazy
from .forms import BookForm
from .models import Book



class Home(TemplateView):
    template_name = 'proteins/home.html'


def upload(request):
    context = {}
    if request.method == 'POST':
        uploaded_file = request.FILES['document']
        fs = FileSystemStorage()
        name = fs.save(uploaded_file.name, uploaded_file)
        url = fs.url(name)
        context['url'] = fs.url(name)
    return render(request, 'proteins/upload.html',context)

def book_list(request):
    books = Book.objects.all()
    return render (request, 'proteins/book_list.html',{'books':books})


def upload_book(request):
    if request.method =='POST':
        form= BookForm(request.POST, request.FILES)
        if form.is_valid():
            form.save()
            return redirect ('book_list')
    else:
        form = BookForm()
    return render (request, 'proteins/upload_book.html', {'form':form})

def delete_book(request, pk):
    if request.method == 'POST':
        book= Book.objects.get(pk=pk)
        book.delete()
        return redirect ('book_list')
class BookList(ListView):
    model = Book
    template_name = 'proteins/class_book_list.html'
    context_object_name ='books'

class UploadBookView(CreateView):
    model= Book
    form_class = BookForm
    success_url = reverse_lazy('class_book_list')
    template_name ='proteins/upload_book.html'